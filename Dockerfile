# For Java 11,
FROM openjdk:17-oracle
WORKDIR /opt/app/car-parking-service
COPY ./target/*.jar /opt/app/car-parking-service/car-parking-service.jar
ENTRYPOINT ["java","-jar","car-parking-service.jar"]