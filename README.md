## Car Park Service
This service is designed to provide information about the closest car parks with to a user with available car parks lot. The application is implemented in Java using the Spring Boot framework and utilizes a PostgreSQL database for storing car park information and availability data.
## Installations
* Apache Maven
* Docker
* Docker Compose
## Approach and Assumptions
* On application initialisation, the class `LoadCarParkSeedData` converts the csv into `CarPark` entity and saves all car parks seed data into the database.
* In order to update car park availability status, we will be using kafka for asynchronous communication and cron job for synchronous communication.
1. Kafka - The `KafkaCarParkConsumerService` will consume the update car park info and will call the `updateCarParkInfo()` method in CarParkService class to update the car park info with its availability slot.
2. Cron Job - The CarParkUpdateScheduler will run every 1 minute to get the car park availability from the official gov site [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability) and then will call the updateCarParkInfo method in CarParkService class to update the car park info with its availability slot.
* In order to convert SVY21 to latitude and longitude format, I have used the `Proj4J` library.
* JQuery is used in CarParkAvailabilityRepository to find the nearest available car park lot.
## Running the Application
1. Clone the Repository: Clone this repository to your local machine using Git.
```bash
git clone git@gitlab.com:ttekglobalassigments/car-park-service.git
```
or
```bash
git clone https://gitlab.com/ttekglobalassigments/car-park-service.git
```
2. Build the Application: Navigate to the project directory and build the application using Maven.
```bash
cd car-park-system
mvn clean install
```
3. Run Docker Containers: Run docker compose file to build and run all services.
```bash
docker-compose up --build
```
### Update Car Park availability using a Kafka
1. Send a request to kafka consumer: Navigate to another terminal and run the following command from kafka producer to send request to kafka consumer
```bash
cat data.json | docker-compose exec -T kafka kafka-console-producer.sh --broker-list kafka:9092 --topic park_availability_topic
```
2. Get nearest car parks: The following api will return all the nearest car parks from your location
```bash
http://localhost:8090/carparks/nearest?latitude=1.37326&longitude=103.897&page=1&perPage=3
```
##### Note: Request parameter latitude and longitude are compulsory fields while page and per page have default values 1 and 50 set respectively.
3. Shut down application: In order to shut down the application, we need to run the following command in order to close all services
```bash
docker-compose down --volumes
```
## Project Structure
- /configuration: Contains configuration files for Kafka consumer and REST template.
- /constants: Stores constant values used across the application.
- /controller: Houses controller classes and exception handlers.
- /dto: Includes Data Transfer Objects.
- /gps: Houses third-party libraries like Proj4J for coordinate conversion.
- /initloader: Contains classes for initializing data.
- /exception: Includes custom exception classes.
- /scheduler: Holds scheduler classes with cron jobs.
- /model: Contains entity models.
- /repository: Includes repository classes for models.
- /service: Consists of service classes containing business logic.
- /util: Includes utility functions utilized throughout the application.