package com.ttek.carparkservice.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto.CarParkData;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto.CarParkInfo;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {KafkaCarParkConsumerServiceImpl.class})
@ExtendWith(SpringExtension.class)
class KafkaCarParkConsumerServiceImplTest {

  @Autowired
  private KafkaCarParkConsumerServiceImpl kafkaCarParkConsumerService;

  @MockBean
  private CarParkAvailabilityService carParkAvailabilityService;

  @MockBean
  private ConsumerRecord consumerRecord;

  @MockBean
  private Acknowledgment acknowledgment;

  private CarParkData carParkData;

  @Autowired
  private ApplicationContext applicationContext;

  @BeforeEach
  public void setUp() {
    kafkaCarParkConsumerService = new KafkaCarParkConsumerServiceImpl(carParkAvailabilityService);
    carParkData = new CarParkData("HKL", LocalDateTime.now().toString(),
        List.of(new CarParkInfo(50, "C", 15)));
  }

  @Test
  void testConsumeEvent_When_EventDTOIsValid_Then_CallExpectedMethods() {
    doReturn(carParkData).when(consumerRecord).value();
    doNothing().when(carParkAvailabilityService).updateCarParkInfo(any());
    kafkaCarParkConsumerService.consumeCarParkAvailabilityEvent(consumerRecord, acknowledgment);
    verify(carParkAvailabilityService, times(1)).updateCarParkInfo(any());
    verify(acknowledgment, times(1)).acknowledge();
  }


  @Test
  void testConsumeResourceCreatedEvent_When_EventDTOIsInvalid_Then_NoAcknowledgment() {
    doReturn(carParkData).when(consumerRecord).value();
    doThrow(new DataAccessException("Failed to access Data") {
      @Override
      public String getMessage() {
        return super.getMessage();
      }
    }).when(carParkAvailabilityService).updateCarParkInfo(any());
    kafkaCarParkConsumerService.consumeCarParkAvailabilityEvent(consumerRecord, acknowledgment);
    verify(carParkAvailabilityService, times(1)).updateCarParkInfo(any());
    verifyNoInteractions(acknowledgment);
  }
}