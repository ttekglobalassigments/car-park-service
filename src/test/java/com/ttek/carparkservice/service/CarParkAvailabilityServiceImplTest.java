package com.ttek.carparkservice.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ttek.carparkservice.dto.CarParkRequestDto;
import com.ttek.carparkservice.dto.CarParkResponseDto;
import com.ttek.carparkservice.model.CarPark;
import com.ttek.carparkservice.repository.CarParkAvailabilityRepository;
import com.ttek.carparkservice.util.CoordinateConverter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CarParkAvailabilityServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CarParkAvailabilityServiceImplTest {

  @MockBean
  private CarParkAvailabilityRepository carParkAvailabilityRepository;

  @MockBean
  private CoordinateConverter coordinateConvertor;

  @Autowired
  private CarParkAvailabilityServiceImpl carParkAvailabilityService;

  private List<CarPark> carParkList = new ArrayList<>();

  private CarParkRequestDto carParkRequestDto = new CarParkRequestDto();

  @BeforeEach
  void setUp() {
    carParkRequestDto = new CarParkRequestDto(1.0, 1.0, 1, 1);
    CarPark carPark = new CarPark("test", "address", 1.0, 103.0);
    carParkList.add(carPark);
  }

  @Test
  void testGetNearestCarParks_When_RequestIsValidAndCarParListIsNotEmpty_Then_CarParkDtoList() {
    Page<CarPark> carParkPage = new PageImpl<>(carParkList);
    when(carParkAvailabilityRepository.findNearestCarParks(anyDouble(), anyDouble(),
        any(Pageable.class))).thenReturn(carParkPage);
    CarParkResponseDto carParkResponseDto = carParkAvailabilityService.getNearestCarParks(
        carParkRequestDto);
    assertEquals(carParkList.size(), carParkResponseDto.getCarParks().size());
    assertEquals(carParkList.size(), carParkResponseDto.getTotalCarParks());
  }

  @Test
  void testGetNearestCarParks_When_RequestIsValidAndCarParkListIsEmpty_Then_EmptyCarParkDtoList() {
    Page<CarPark> carParkPage = new PageImpl<>(new ArrayList<>());
    when(carParkAvailabilityRepository.findNearestCarParks(anyDouble(), anyDouble(),
        any(Pageable.class))).thenReturn(carParkPage);
    CarParkResponseDto carParkResponseDto = carParkAvailabilityService.getNearestCarParks(
        carParkRequestDto);
    assertEquals(0, carParkResponseDto.getCarParks().size());
    assertEquals(0, carParkResponseDto.getTotalCarParks());
  }

  @Test
  void testSaveAllCarParks_When_CarParkListIsValid_Return() {
    carParkAvailabilityService.saveAll(carParkList);
    verify(carParkAvailabilityRepository, times(1)).saveAll(any());
  }

  @Test
  void testFindByCarParkNumber_When_CarParkNumberExists_ReturnCarPark() {
    when(carParkAvailabilityRepository.findOneByCarParkNo(any())).thenReturn(carParkList.get(0));
    carParkAvailabilityService.findByCarParkNumber("carParkList");
    verify(carParkAvailabilityRepository, times(1)).findOneByCarParkNo(any());
  }

  @Test
  void testSaveCarPark_When_CarParkListIsValid_Return() {
    carParkAvailabilityService.save(carParkList.get(0));
    verify(carParkAvailabilityRepository, times(1)).save(any());
  }
}