package com.ttek.carparkservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ttek.carparkservice.constants.ErrorCode;
import com.ttek.carparkservice.constants.ExceptionMessage;
import com.ttek.carparkservice.dto.BaseErrorResponseDto;
import com.ttek.carparkservice.dto.CarParkRequestDto;
import com.ttek.carparkservice.exceptions.LoadSeedDataException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.BindException;

@ContextConfiguration(classes = {ControllerExceptionHandler.class})
@ExtendWith(SpringExtension.class)
class ControllerExceptionalHandlerTest {

  @Autowired
  private ControllerExceptionHandler controllerExceptionHandler;

  @ParameterizedTest
  @ValueSource(strings = " nested exception is java.lang.IllegalArgumentException: Invalid value double value")
  void testHandleBindException(String exceptionMessage) {
    CarParkRequestDto carParkRequestDto = new CarParkRequestDto();
    BindException exception = new BindException(carParkRequestDto, "carParkRequestDto");
    exception.rejectValue("latitude", "400", exceptionMessage);
    ResponseEntity<List<BaseErrorResponseDto>> response = controllerExceptionHandler.handleBindException(
        exception);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertEquals(ErrorCode.BAD_REQUEST.toString(), response.getBody().get(0).getCode());
    assertEquals(1, response.getBody().size());
  }

  @Test
  void testHandleLoadSeedDataException() {
    LoadSeedDataException exception = new LoadSeedDataException(
        ExceptionMessage.CSV_PROCESSING_ERROR);
    assert controllerExceptionHandler != null;
    ResponseEntity<List<BaseErrorResponseDto>> response = controllerExceptionHandler.handleLoadSeedDataException(
        exception);
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
    assertEquals(ErrorCode.UNPROCESSABLE_ENTITY.toString(), response.getBody().get(0).getCode());
    assertEquals(1, response.getBody().size());
  }
}