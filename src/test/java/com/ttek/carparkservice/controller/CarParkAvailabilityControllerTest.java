package com.ttek.carparkservice.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.ttek.carparkservice.dto.CarParkDto;
import com.ttek.carparkservice.dto.CarParkResponseDto;
import com.ttek.carparkservice.model.CarPark;
import com.ttek.carparkservice.service.CarParkAvailabilityService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {CarParkAvailabilityController.class})
@ExtendWith(SpringExtension.class)
class CarParkAvailabilityControllerTest {

  MockHttpServletRequestBuilder requestBuilder;
  @Autowired
  private CarParkAvailabilityController carParkAvailabilityController;
  @MockBean
  private CarParkAvailabilityService carParkAvailabilityService;
  @MockBean
  private ControllerExceptionHandler controllerExceptionHandler;

  private CarParkResponseDto carParkResponseDto;

  private static final String NEAREST_CAR_PARK_API = "/carparks/nearest?";

  @BeforeEach
  void setUp() {
    controllerExceptionHandler = new ControllerExceptionHandler();
    CarPark carPark = new CarPark("test", "address", 1.0, 103.0);
    CarParkDto carParkDto = new CarParkDto(carPark);
    List<CarParkDto> carParkDtos = new ArrayList<>();
    carParkDtos.add(carParkDto);
    carParkResponseDto = new CarParkResponseDto(carParkDtos, 1);
  }

  @Test
  void testGetNearestCarParks_When_RequestIsValid_Then_ReturnOk() throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "latitude=1.0&longitude=1.0");
    when(carParkAvailabilityService.getNearestCarParks(any())).thenReturn(carParkResponseDto);
    String XTotalCountHeader = "X-Total-Count";
    MockMvcBuilders.standaloneSetup(carParkAvailabilityController)
        .setControllerAdvice(controllerExceptionHandler).build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.header()
            .stringValues(XTotalCountHeader, String.valueOf(carParkResponseDto.getTotalCarParks())));
  }

  @Test
  void testGetNearestCarParks_When_RequestIsValidAndResponseIsNull_Then_ReturnNoContent()
      throws Exception {
    CarParkResponseDto carParkResponseDto = new CarParkResponseDto(new ArrayList<>(), 0);
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "latitude=1.0&longitude=1.0");
    when(carParkAvailabilityService.getNearestCarParks(any())).thenReturn(carParkResponseDto);
    String XTotalCount = "X-Total-Count";
    MockMvcBuilders.standaloneSetup(carParkAvailabilityController)
        .setControllerAdvice(controllerExceptionHandler).build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isNoContent()).andExpect(
            MockMvcResultMatchers.header()
                .stringValues(XTotalCount, String.valueOf(carParkResponseDto.getTotalCarParks())));
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "test"})
  void testGetNearestCarParks_When_PageIsInvalid_Then_ReturnBadRequest(String page)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "longitude=1.0&latitude=1.0&page=" + page);
    MockMvcBuilders.standaloneSetup(carParkAvailabilityController)
        .setControllerAdvice(controllerExceptionHandler).build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "test"})
  void testGetNearestCarParks_When_PerPageIsInvalid_Then_ReturnBadRequest(String page)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "longitude=1.0&latitude=1.0&page=1&perPage=" + page);
    MockMvcBuilders.standaloneSetup(carParkAvailabilityController)
        .setControllerAdvice(controllerExceptionHandler).build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }


  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test", "100", "-500"})
  void testGetNearestCarParks_When_LatitudeIsInvalidOrNull_Then_ReturnBadRequest(String latitude)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "longitude=1.0&latitude=" + latitude);
    MockMvcBuilders.standaloneSetup(carParkAvailabilityController)
        .setControllerAdvice(controllerExceptionHandler).build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test", "500", "-500"})
  void testGetNearestCarParks_When_LongitudeIsInvalidOrNull_Then_ReturnBadRequest(String longitude)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "latitude=1.0&longitude=" + longitude);
    MockMvcBuilders.standaloneSetup(carParkAvailabilityController)
        .setControllerAdvice(controllerExceptionHandler).build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }
}