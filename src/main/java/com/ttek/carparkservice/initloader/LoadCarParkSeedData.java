package com.ttek.carparkservice.initloader;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import com.ttek.carparkservice.constants.ExceptionMessage;
import com.ttek.carparkservice.model.CarPark;
import com.ttek.carparkservice.service.CarParkAvailabilityService;
import com.ttek.carparkservice.util.CoordinateConverter;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class LoadCarParkSeedData {

  private final CarParkAvailabilityService carParkAvailabilityService;

  private final ResourceLoader resourceLoader;

  private final CoordinateConverter coordinateConverter;

  @PostConstruct
  public void init() {
    try {
      Resource resource = resourceLoader.getResource("classpath:HDBCarParkInformation.csv");
      List<CarPark> carParks = readAndProcessCSV(resource);
      carParkAvailabilityService.saveAll(carParks);
    } catch (IOException | CsvValidationException e) {
      log.error(String.format(ExceptionMessage.CSV_PROCESSING_ERROR, e.getMessage()));
    }
  }

  private List<CarPark> readAndProcessCSV(Resource resource)
      throws IOException, CsvValidationException {
    List<CarPark> carParks = new ArrayList<>();
    try (CSVReader csvReader = new CSVReaderBuilder(
        new InputStreamReader(resource.getInputStream())).withSkipLines(1).build()) {
      String[] nextRecord;

      log.debug("Processing coordinates and converting to Latitude and Latitude");
      while ((nextRecord = csvReader.readNext()) != null) {
        String carParkNo = nextRecord[0];
        String address = nextRecord[1];
        double xCoord = Double.parseDouble(nextRecord[2]);
        double yCoord = Double.parseDouble(nextRecord[3]);

//      //Convert SVY21 to latitude and longitude
        double[] latlon = coordinateConverter.getLatLon(xCoord, yCoord);
        carParks.add(new CarPark(carParkNo, address, latlon[0], latlon[1]));
      }
    } catch (IOException | CsvValidationException e) {
      log.error(String.format(ExceptionMessage.CSV_PROCESSING_ERROR, e.getMessage()));
    }
    return carParks;
  }
}