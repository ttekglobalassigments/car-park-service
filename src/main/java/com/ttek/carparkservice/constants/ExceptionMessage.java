package com.ttek.carparkservice.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionMessage {

  public static final String UPDATE_CAR_PARK_ERROR = "Error occurred while updating car park lot : %s";
  public static final String CSV_PROCESSING_ERROR = "Error occurred while processing CSV file: %s";
  public static final String SCHEDULER_OPERATION_FAILED = "Scheduler not able to update car park availability info.";
}