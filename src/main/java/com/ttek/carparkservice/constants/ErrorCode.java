package com.ttek.carparkservice.constants;

public enum ErrorCode {
  BAD_REQUEST, UNPROCESSABLE_ENTITY
}