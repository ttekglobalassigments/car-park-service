package com.ttek.carparkservice.gps;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.locationtech.proj4j.CRSFactory;
import org.locationtech.proj4j.CoordinateReferenceSystem;
import org.locationtech.proj4j.CoordinateTransform;
import org.locationtech.proj4j.CoordinateTransformFactory;
import org.locationtech.proj4j.ProjCoordinate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SVY21ToWGS84Converter {

  private static final String SVY21_PROJ = "+proj=tmerc +lat_0=1.366666 +lon_0=103.833333 +k=1 +x_0=28001.642 +y_0=38744.572 +ellps=WGS84 +units=m +no_defs";
  private static final String WGS84_PROJ = "+proj=longlat +datum=WGS84 +no_defs";

  /**
   * Converts coordinates from SVY21 (Singapore) projection to WGS84 (World Geodetic System 1984)
   * projection.
   *
   * @param svy21E The easting (X-coordinate) in SVY21 projection.
   * @param svy21N The northing (Y-coordinate) in SVY21 projection.
   * @return A double array containing the latitude (index 0) and longitude (index 1) in WGS84
   * projection.
   */
  public static double[] convertSVY21ToWGS84(double svy21E, double svy21N) {
    ProjCoordinate svy21Coord = new ProjCoordinate(svy21E, svy21N);
    CoordinateTransformFactory ctFactory = new CoordinateTransformFactory();
    CRSFactory crsFactory = new CRSFactory();
    CoordinateReferenceSystem svy21CRS = crsFactory.createFromParameters("SVY21", SVY21_PROJ);
    CoordinateReferenceSystem wgs84CRS = crsFactory.createFromParameters("WGS84", WGS84_PROJ);
    CoordinateTransform transform = ctFactory.createTransform(svy21CRS, wgs84CRS);

    ProjCoordinate wgs84Coord = new ProjCoordinate();
    transform.transform(svy21Coord, wgs84Coord);

    return new double[]{wgs84Coord.y, wgs84Coord.x}; // Return latitude and longitude
  }
}