package com.ttek.carparkservice.controller;

import com.ttek.carparkservice.constants.ErrorCode;
import com.ttek.carparkservice.dto.BaseErrorResponseDto;
import com.ttek.carparkservice.exceptions.LoadSeedDataException;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(value = {BindException.class})
  protected ResponseEntity<List<BaseErrorResponseDto>> handleBindException(BindException ex) {
    List<BaseErrorResponseDto> errors = nestedExceptionHandler(ex);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
  }

  private List<BaseErrorResponseDto> nestedExceptionHandler(BindException exception) {
    String illegalArgumentException = "nested exception is java.lang.IllegalArgumentException";
    String numberFormatException = "nested exception is java.lang.NumberFormatException";

    return exception.getBindingResult().getFieldErrors().stream().map(fieldError -> {
      String defaultMessage = fieldError.getDefaultMessage();
      assert defaultMessage != null;
      if (defaultMessage.contains(illegalArgumentException)) {
        String exceptionMessage = defaultMessage.split(illegalArgumentException)[1].trim();
        exceptionMessage = exceptionMessage.substring(2);
        return new BaseErrorResponseDto(ErrorCode.BAD_REQUEST.toString(), exceptionMessage);
      } else if (defaultMessage.contains(numberFormatException)) {
        String exceptionMessage = "Incorrect value provided for field : " + fieldError.getField();
        return new BaseErrorResponseDto(HttpStatus.BAD_REQUEST.toString(), exceptionMessage);
      }
      return new BaseErrorResponseDto(HttpStatus.BAD_REQUEST.toString(), defaultMessage);
    }).toList();
  }

  @ExceptionHandler(value = {LoadSeedDataException.class})
  protected ResponseEntity<List<BaseErrorResponseDto>> handleLoadSeedDataException(
      LoadSeedDataException exception) {
    return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(List.of(
        new BaseErrorResponseDto(ErrorCode.UNPROCESSABLE_ENTITY.toString(), exception.getMessage())));
  }
}