package com.ttek.carparkservice.controller;

import com.ttek.carparkservice.dto.CarParkDto;
import com.ttek.carparkservice.dto.CarParkRequestDto;
import com.ttek.carparkservice.dto.CarParkResponseDto;
import com.ttek.carparkservice.service.CarParkAvailabilityService;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carparks")
public class CarParkAvailabilityController {

  private final CarParkAvailabilityService carParkAvailabilityService;

  /**
   * Retrieves a list of nearest car parks based on the provided criteria.
   *
   * @param carParkRequestDto The DTO containing parameters for finding nearest car parks.
   * @return A ResponseEntity containing a list of CarParkDto objects representing the nearest car
   * parks.
   */
  @GetMapping("/nearest")
  public ResponseEntity<List<CarParkDto>> getNearestCarParks(
      @Valid CarParkRequestDto carParkRequestDto) {
    CarParkResponseDto carParkResponseDto = carParkAvailabilityService.getNearestCarParks(
        carParkRequestDto);
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set("X-Total-Count", String.valueOf(carParkResponseDto.getTotalCarParks()));
    return carParkResponseDto.getCarParks().isEmpty() ? new ResponseEntity<>(responseHeaders,
        HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(carParkResponseDto.getCarParks(), responseHeaders, HttpStatus.OK);
  }
}