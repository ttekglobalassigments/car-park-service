package com.ttek.carparkservice.exceptions;

public class LoadSeedDataException extends RuntimeException {

  public LoadSeedDataException(String message) {
    super(message);
  }
}