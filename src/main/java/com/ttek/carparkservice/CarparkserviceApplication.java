package com.ttek.carparkservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class CarparkserviceApplication {

  public static void main(String[] args) {
    SpringApplication.run(CarparkserviceApplication.class, args);
  }
}