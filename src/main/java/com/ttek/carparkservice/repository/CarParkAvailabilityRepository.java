package com.ttek.carparkservice.repository;

import com.ttek.carparkservice.model.CarPark;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkAvailabilityRepository extends PagingAndSortingRepository<CarPark, Long> {

  List<CarPark> findByAvailableLotsNot(int excludedAvailableLots);

  CarPark findOneByCarParkNo(String carParkNumber);

  @Query("SELECT c FROM CarPark c WHERE c.availableLots > 0 ORDER BY FUNCTION('SQRT', "
      + "(FUNCTION('POWER', (c.latitude - :latitude), 2) + FUNCTION('POWER', (c.longitude - :longitude), 2)))")
  Page<CarPark> findNearestCarParks(@Param("latitude") double latitude,
      @Param("longitude") double longitude, Pageable pageable);

  CarPark save(CarPark carPark);

  Iterable<CarPark> saveAll(Iterable<CarPark> carParks);
}