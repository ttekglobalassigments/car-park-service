package com.ttek.carparkservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ttek.carparkservice.model.CarPark;

public class CarParkDto {

  @JsonProperty("address")
  private String address;

  @JsonProperty("latitude")
  private double latitude;

  @JsonProperty("longitude")
  private double longitude;

  @JsonProperty("total_lots")
  private int totalLots;

  @JsonProperty("available_lots")
  private int availableLots;

  public CarParkDto(CarPark carPark) {
    this.address = carPark.getAddress();
    this.latitude = carPark.getLatitude();
    this.longitude = carPark.getLongitude();
    this.totalLots = carPark.getTotalLots();
    this.availableLots = carPark.getAvailableLots();
  }
}