package com.ttek.carparkservice.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarParkRequestDto {

  @NotNull(message = "Latitude cannot be null")
  @Min(value = -90, message = "Latitude should not be less than -90")
  @Max(value = 90, message = "Latitude should not be greater than 90")
  private Double latitude;

  @NotNull(message = "Longitude cannot be null")
  @Min(value = -180, message = "Longitude should not be less than -180")
  @Max(value = 180, message = "Longitude should not be greater than 180")
  private Double longitude;

  @Min(value = 1, message = "Page value should be greater than or equal to 1")
  private Integer page = 1;

  @Min(value = 1, message = "Per page value should be greater than or equal to 1")
  private Integer perPage = 50;
}