package com.ttek.carparkservice.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarParkAvailabilityDto {

  private List<Item> items;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Item {

    private String timestamp;
    private List<CarParkData> carpark_data;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class CarParkData {

    private String carpark_number;
    private String update_datetime;
    private List<CarParkInfo> carpark_info;

  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class CarParkInfo {

    private int total_lots;
    private String lot_type;
    private int lots_available;
  }
}