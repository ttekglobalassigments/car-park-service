package com.ttek.carparkservice.util;

import com.ttek.carparkservice.gps.SVY21ToWGS84Converter;
import org.springframework.stereotype.Component;

@Component
public class CoordinateConverter {

  public double[] getLatLon(double xCoord, double yCoord) {
    return SVY21ToWGS84Converter.convertSVY21ToWGS84(xCoord, yCoord);
  }
}