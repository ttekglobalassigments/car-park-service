package com.ttek.carparkservice.scheduler;

import com.ttek.carparkservice.configuration.RestTemplateConfiguration;
import com.ttek.carparkservice.constants.ExceptionMessage;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto;
import com.ttek.carparkservice.service.CarParkAvailabilityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
@RequiredArgsConstructor
public class CarParkInfoUpdateScheduler {

  private final RestTemplateConfiguration restTemplateConfiguration;

  private final CarParkAvailabilityService carParkAvailabilityService;

  @Value("${com.ttek.carparkservice.carpark-availability.url}")
  private String carParkAvailabilityUrl;

  /**
   * Executes a scheduled task with a cron expression to update car park information.
   */
  @Scheduled(cron = "${scheduled.cron-expression}")
  public void scheduledTaskWithCronExpression() {
    try {
      log.debug("Starting to update car park info via cron job");

      // Build the URL with query parameters if needed
      UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(carParkAvailabilityUrl);

      // Make the GET request
      ResponseEntity<CarParkAvailabilityDto> responseEntity = restTemplateConfiguration.restTemplate()
          .getForEntity(builder.toUriString(), CarParkAvailabilityDto.class);

      // Extract the response body
      CarParkAvailabilityDto carParkAvailabilityDto = responseEntity.getBody();

      //update car park info in database
      carParkAvailabilityService.updateCarParkInfo(carParkAvailabilityDto);

      log.debug("Updated car park info via cron job");
    } catch (RuntimeException exception) {
      log.error(String.format(ExceptionMessage.SCHEDULER_OPERATION_FAILED, exception.getMessage()));
    }
  }
}