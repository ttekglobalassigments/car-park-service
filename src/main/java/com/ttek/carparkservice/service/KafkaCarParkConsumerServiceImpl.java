package com.ttek.carparkservice.service;

import com.ttek.carparkservice.constants.ExceptionMessage;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto.CarParkData;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto.Item;
import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.dao.DataAccessException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class KafkaCarParkConsumerServiceImpl implements KafkaCarParkConsumerService {

  private final CarParkAvailabilityService carParkAvailabilityService;

  /**
   * Listens for car park availability events from Kafka and consumes them.
   *
   * @param consumerRecord The Kafka ConsumerRecord containing the car park availability event.
   * @param acknowledgment The acknowledgment object used to acknowledge the processing of the
   *                       message.
   */
  @Override
  @KafkaListener(topics = "${com.ttek.carparkservice.kafka.topic}", containerFactory = "kafkaListenerContainerFactory")
  public void consumeCarParkAvailabilityEvent(ConsumerRecord<String, CarParkData> consumerRecord,
      Acknowledgment acknowledgment) {

    try {
      CarParkData carParkData = consumerRecord.value();
      log.debug("Received update car info event for car park no : {}",
          carParkData.getCarpark_number());
      Item item = new Item(LocalDateTime.now().toString(), List.of(carParkData));
      CarParkAvailabilityDto carParkAvailabilityDetailDTO = new CarParkAvailabilityDto(
          List.of(item));
      carParkAvailabilityService.updateCarParkInfo(carParkAvailabilityDetailDTO);
      acknowledgment.acknowledge();
    } catch (DataAccessException exception) {
      log.error(String.format(ExceptionMessage.UPDATE_CAR_PARK_ERROR, exception));
    }
  }
}