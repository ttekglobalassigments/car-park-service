package com.ttek.carparkservice.service;

import com.ttek.carparkservice.dto.CarParkDto;
import com.ttek.carparkservice.dto.CarParkRequestDto;
import com.ttek.carparkservice.dto.CarParkResponseDto;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto;
import com.ttek.carparkservice.model.CarPark;
import com.ttek.carparkservice.repository.CarParkAvailabilityRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CarParkAvailabilityServiceImpl implements CarParkAvailabilityService {

  private final CarParkAvailabilityRepository carParkAvailabilityRepository;

  @Override
  public CarParkResponseDto getNearestCarParks(CarParkRequestDto carParkQueryDto) {
    List<CarParkDto> carParkDtos = new ArrayList<>();

    // Create a pageable object for pagination
    Pageable pageable = PageRequest.of(carParkQueryDto.getPage() - 1, carParkQueryDto.getPerPage());

    // Fetch the page of nearest car parks with available lots
    Page<CarPark> carParkPage = carParkAvailabilityRepository.findNearestCarParks(
        carParkQueryDto.getLatitude(), carParkQueryDto.getLongitude(), pageable);

    // Convert the page of entities to DTOs
    carParkPage.forEach(carPark -> carParkDtos.add(new CarParkDto(carPark)));

    return new CarParkResponseDto(carParkDtos, carParkPage.getTotalElements());
  }

  @Override
  public CarPark findByCarParkNumber(String carParkNumber) {
    return carParkAvailabilityRepository.findOneByCarParkNo(carParkNumber);
  }

  @Override
  public void save(CarPark carPark) {
    carParkAvailabilityRepository.save(carPark);
  }

  @Override
  public void saveAll(List<CarPark> carParks) {
    carParkAvailabilityRepository.saveAll(carParks);
  }

  @Override
  public synchronized void updateCarParkInfo(CarParkAvailabilityDto carParkAvailabilityDto) {
    for (CarParkAvailabilityDto.Item item : carParkAvailabilityDto.getItems()) {
      for (CarParkAvailabilityDto.CarParkData carParkData : item.getCarpark_data()) {
        CarPark carPark = findByCarParkNumber(carParkData.getCarpark_number());
        if (carPark != null) {
          carParkData.getCarpark_info().forEach(carParkInfo -> {
            carPark.setAvailableLots(carParkInfo.getLots_available());
            carPark.setTotalLots(carParkInfo.getTotal_lots());
            carParkAvailabilityRepository.save(carPark);
          });
        }
      }
    }
  }
}