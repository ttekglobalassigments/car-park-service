package com.ttek.carparkservice.service;

import com.ttek.carparkservice.dto.CarParkAvailabilityDto.CarParkData;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;

public interface KafkaCarParkConsumerService {

  /**
   * Accepts and performs operation to update car park info received from the kafka producer.
   *
   * @param consumerRecord consists of consumed record from kafka event sent.
   * @param acknowledgment consists of acknowledgment of kafka event.
   */
  void consumeCarParkAvailabilityEvent(ConsumerRecord<String, CarParkData> consumerRecord,
      Acknowledgment acknowledgment);
}