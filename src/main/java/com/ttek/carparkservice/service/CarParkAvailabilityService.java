package com.ttek.carparkservice.service;

import com.ttek.carparkservice.dto.CarParkRequestDto;
import com.ttek.carparkservice.dto.CarParkResponseDto;
import com.ttek.carparkservice.dto.CarParkAvailabilityDto;
import com.ttek.carparkservice.model.CarPark;
import java.util.List;

public interface CarParkAvailabilityService {

  /**
   * Computes the nearest car parks from given latitude and longitude.
   * <p>
   * This method returns CarParkResponseDto object that contains two fields, carParks which is list
   * of car parks, and totalCarParks, which consists of total car parks received.
   *
   * @param carParkRequestDto consists of all the request parameters.
   * @return the result an CarParkDtoList.
   */
  CarParkResponseDto getNearestCarParks(CarParkRequestDto carParkRequestDto);

  /**
   * Returns all car parks from your coordinate.
   * <p>
   * This method returns List of CarPark object which consists of all the car park details.
   *
   * @return the result is List of CarPark.
   */

  void saveAll(List<CarPark> carParks);

  /**
   * Returns a car park consisting of the specified car park number.
   *
   * @param carParkNumber consists of the car park number or identifier.
   * @return the result is the updated car park detail.
   */
  CarPark findByCarParkNumber(String carParkNumber);

  /**
   * Saves the provided car park entity to the database.
   *
   * @param carPark The car park entity to be saved.
   */
  void save(CarPark carPark);

  /**
   * Updates the car park information in the database based on the provided car park availability
   * DTO.
   *
   * @param carParkAvailabilityDto The DTO containing updated car park availability information.
   */
  void updateCarParkInfo(CarParkAvailabilityDto carParkAvailabilityDto);
}