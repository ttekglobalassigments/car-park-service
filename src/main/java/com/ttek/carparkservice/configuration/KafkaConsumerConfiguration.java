package com.ttek.carparkservice.configuration;

import com.ttek.carparkservice.dto.CarParkAvailabilityDto.CarParkData;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties.AckMode;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@Configuration
@EnableKafka
public class KafkaConsumerConfiguration {

  @Value("${com.ttek.carparkservice.kafka-bootstrap-server}")
  private String kafkaBootstrapServer;

  @Bean
  public ConsumerFactory<String, CarParkData> consumerFactory() {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBootstrapServer);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "group_id");
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

    // Use JsonDeserializer for value deserialization
    JsonDeserializer<CarParkData> jsonDeserializer = new JsonDeserializer<>(CarParkData.class);
    jsonDeserializer.addTrustedPackages("*");

    return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), // Key deserializer
        jsonDeserializer // Value deserializer
    );
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, CarParkData> kafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, CarParkData> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    factory.getContainerProperties().setAckMode(AckMode.MANUAL_IMMEDIATE);
    return factory;
  }
}