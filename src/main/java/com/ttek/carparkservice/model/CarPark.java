package com.ttek.carparkservice.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CarPark {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(nullable = false)
  private String address;

  @Column(nullable = false)
  private Double latitude;

  @Column(nullable = false)
  private Double longitude;

  private int totalLots;

  private int availableLots;

  @Column(unique = true)
  private String carParkNo;

  @Transient
  private double distance;

  public CarPark(String carParkNo, String address, double latitude, double longitude) {
    this.carParkNo = carParkNo;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}